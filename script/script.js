
//  Our Services 
$('ul.services-tabs').on('click', 'li:not(.active)', function() {
    let id = $(this).attr('data-target'),                           //Получили значение атрибута data-target у текущего элемента
       content = $('.tabs-content-item[data-target="'+ id +'"]');   //конкатенация строк чтобы получить .tabs-content-item[data-target="нажатый елемент"]
   
   $('.services-item.active').removeClass('active'); 
   $(this).addClass('active'); 
   
   $('.tabs-content-item.active').removeClass('active'); 
   content.addClass('active'); 
  });

  //load more

$('.btn-load-more').on('click', function() {
    $('.work-img').addClass('active');
    $('.work-img').data('all' , 'All');
    $('.btn-load-more').remove();
    content.show();
});

// фильтрация продукции 

const content = $('.work-img');

$('.item-amazing-work').click(function() { 
    let tab = $(this).data('photo');
    $('.item-amazing-work').removeClass('activeTab')
    $(this).addClass('activeTab')
    content
        .hide()
        .filter(function () {
            return $(this).data('content') === tab || $(this).data('all') === tab;
        })
        .show();
});


// карусель

let listItems = $(".content-item");
let tabItems = $(".tabs-item-people");
let listLen = listItems.length;
let current;

function moveTo(newIndex) {
let i = newIndex;
if (newIndex == 'back') {
  i = (current > 0) ? (current - 1) : (listLen - 1);
}

if (newIndex == 'next') {
  i = (current < listLen - 1) ? (current + 1) : 0;
}

tabItems.removeClass('active-people')
      .eq(i).addClass('active-people');

listItems.removeClass('active-people')
.eq(i).addClass('active-people');

current = i;
};


$(".tabs-item-people").click(function () {
    let i = $('.tabs-item-people').index(this);
    moveTo(i);
  });

  $("#back").click(function () {
    moveTo('back');
  });

  $("#next").click(function () {
    moveTo('next');
  });
  
  moveTo('next');
